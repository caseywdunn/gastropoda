#!/bin/sh
#SBATCH -t 14-00:00:00
#SBATCH -N 16
#SBATCH -c 16
#SBATCH --mem=60g
#SBATCH -C e5-2600
#SBATCH --exclusive

module load raxml

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-gastropoda.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-gastropoda.sqlite,outdir=/gpfs/data/cdunn/analyses/gastropoda,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"
export BIOLITE_HOSTLIST=$(hostlist -e -s, $SLURM_NODELIST)

ID=GastropodaPhylogeny

LOAD_IDS=$(agalma diagnostics runid -n load)

cd ~/scratch
mkdir -p $ID
cd $ID

agalma homologize --id $ID $LOAD_IDS
#Uncomment the following line (and comment the previous line) if running OMA. OMA requires some extra-processing outside of Agalma due to interaction with the filesystem 
#agalma orthologize -- id $ID $ID $LOAD_IDS
agalma multalign --id $ID
agalma genetree --id $ID

