#!/bin/sh
#SBATCH -t 24:00:00
#SBATCH -c 8
#SBATCH --mem=20G
#SBATCH --exclusive
#SARRAY --range=1-7

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-gastropoda.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-gastropoda.sqlite,outdir=/gpfs/data/cdunn/analyses/gastropoda,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"


IDS=(
	454_BIOGLA
	454_ILYOBS
	454_LITLIT
	JGI_LOTGI1
	454_PERLUC
	OIST_PINFUC
	454_SIPPEC
)

ID=${IDS[$SLURM_ARRAYID-1]}
echo $ID
agalma catalog search $ID

cd ~/scratch
mkdir -p $ID
cd $ID

agalma postassemble --id $ID
agalma load --id $ID --generic
