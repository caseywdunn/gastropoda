#!/bin/sh
#SBATCH -t 48:00:00
#SBATCH -c 16
#SBATCH --mem=60g
#SBATCH -C e5-2600
#SBATCH --exclusive

module load raxml

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-gastropoda.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-gastropoda.sqlite,outdir=/gpfs/data/cdunn/analyses/gastropoda,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

ID=GastropodaPhylogeny

cd ~/scratch
mkdir -p $ID
cd $ID

agalma treeprune --id $ID
agalma multalign --id $ID
agalma supermatrix --id $ID #--proportion 0.7
