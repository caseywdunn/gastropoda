#!/bin/sh

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-gastropoda.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-gastropoda.sqlite,outdir=/gpfs/data/cdunn/analyses/gastropoda"


mkdir -p ~/data
cd ~/data

EMAIL="felipe_zapata@brown.edu"

sra import -c -e $EMAIL SRR1505101 &
sra import -c -e $EMAIL SRR1505102 &
sra import -c -e $EMAIL SRR1505103 &
sra import -c -e $EMAIL SRR1505104 &
sra import -c -e $EMAIL SRR1505105 &
sra import -c -e $EMAIL SRR1505107 &
sra import -c -e $EMAIL SRR1505108 &
sra import -c -e $EMAIL SRR1505109 &
sra import -c -e $EMAIL SRR1505110 &
sra import -c -e $EMAIL SRR1505111 &
sra import -c -e $EMAIL SRR1505112 &
sra import -c -e $EMAIL SRR1505113 &
sra import -c -e $EMAIL SRR1505114 &
sra import -c -e $EMAIL SRR1505115 &
sra import -c -e $EMAIL SRR1505116 &
sra import -c -e $EMAIL SRR1505117 &
sra import -c -e $EMAIL SRR1505118 &
sra import -c -e $EMAIL SRR1505119 &
sra import -c -e $EMAIL SRR1505120 &
sra import -c -e $EMAIL SRR1505121 &
sra import -c -e $EMAIL SRR1505122 &
sra import -c -e $EMAIL SRR1505123 &
sra import -c -e $EMAIL SRR1505124 &
sra import -c -e $EMAIL SRR1505125 &
sra import -c -e $EMAIL SRR1505126 &
sra import -c -e $EMAIL SRR1505127 &
sra import -c -e $EMAIL SRR1505128 &
sra import -c -e $EMAIL SRR1505129 &
sra import -c -e $EMAIL SRR1505130 &
sra import -c -e $EMAIL SRR1505131 &
sra import -c -e $EMAIL SRR1505132 &
sra import -c -e $EMAIL SRR1505133 &
sra import -c -e $EMAIL SRR1505134 &
sra import -c -e $EMAIL SRR1505135 &
sra import -c -e $EMAIL SRR1505136 &
sra import -c -e $EMAIL SRR1505137 &
sra import -c -e $EMAIL SRR1505138 &
sra import -c -e $EMAIL SRR1505139 &
sra import -c -e $EMAIL SRR1505140 &
sra import -c -e $EMAIL SRR1505141 &
wait

agalma catalog insert --id "D03U4ACXX110626-APLYSIA" --paths "/gpfs/data/cdunn/sequences/public/SRA/Aplysia_californica/D03U4ACXX110626_1.fq.gz" --species "Aplysia californica" --ncbi_id "6500" --itis_id "78032" --library_type "transcriptome" --sequencer "Ilumina HiSeq 2000" --note "Libraries from different tissues were pulled together for a more complete transcriptome. Reverse reads were NOT used (very bad quality). Treated as SE"
agalma catalog insert --id "454_BIOGLA" --paths "/gpfs/data/cdunn/sequences/public/SRA/Biomphalaria_glabrata_Newbler/454Exemplar.fasta" --species "Biomphalaria glabrata" --ncbi_id "6526" --itis_id "76637" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 data by Sonia from SRA archive" --sample_prep " | "
agalma catalog insert --id "ILLUMINA-3AB384_0041_FC-7-CHITON" --paths "/gpfs/data/cdunn/sequences/collaborators/mollusca_harvard_09Sep2011/Colivaceus/Colivaceus_s_7_1_sequence.q33.trim.fastq.gz:/gpfs/data/cdunn/sequences/collaborators/mollusca_harvard_09Sep2011/Colivaceus/Colivaceus_s_7_2_sequence.q33.trim.fastq.gz" --species "Chiton olivaceus" --ncbi_id "256108" --itis_id "79012" --library_type "transcriptome" --note "Trimmed to 80." --sample_prep "Trizol | Dynabeads mRNA Purification Kit | 2 rounds | Vollmer" --sequencer "Illumina Genome Analyzer II"
agalma catalog insert --id "EAS1745-2-ENNUCULA" --paths "/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Ennucula_tenuis/s_2_1_sequence.q33.fastq.gz:/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Ennucula_tenuis/s_2_2_sequence.q33.fastq.gz" --species "Ennucula tenuis" --ncbi_id "106224" --itis_id "567536" --extraction_id "FEG265" --library_id "FEG265" --library_type "transcriptome" --sequencer "Illumina Genome Analyzer IIx" --seq_center "Brown Genomics Core" --note "No trimming necessary" --sample_prep "Trizol | Invitrogen Dynabeads mRNA Purification Kit ; 2 rounds | NEBNext RNA Sample Prep. Reagent Set Set 1"
agalma catalog insert --id "EAS1745-3-GADILA" --paths "/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Gadila_tolmiei/s_3_1_sequence.q33.fastq.gz:/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Gadila_tolmiei/s_3_2_sequence.q33.fastq.gz" --species "Gadila tolmiei" --ncbi_id "1077242" --itis_id "82321" --extraction_id "FEG267" --library_id "FEG267" --library_type "transcriptome" --sequencer "Illumina Genome Analyzer IIx" --seq_center "Brown Genomics Core" --note "No trimming necessary" --sample_prep "Trizol | Invitrogen Dynabeads mRNA Purification Kit ; 2 rounds | NEBNext RNA Sample Prep. Reagent Set Set 1"
agalma catalog insert --id "EAS1745-6-GREENLAND" --paths "/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Greenland_neomeniomorph/s_6_1_sequence.q33.trim.fastq.gz:/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Greenland_neomeniomorph/s_6_2_sequence.q33.trim.fastq.gz" --species "Greenland neomeniomorph" --ncbi_id "1077253" --itis_id "566845" --extraction_id "FEG268" --library_id "FEG268" --library_type "transcriptome" --sequencer "Illumina Genome Analyzer IIx" --seq_center "Brown Genomics Core" --note "Trimmed to 90" --sample_prep "NEB Magnetic mRNA Isolation Kit ; 1 round | NEBNext RNA Sample Prep. Reagent Set Set 1"
agalma catalog insert --id "HWI-ST700693-172-HALIOTIS" --paths "/gpfs/data/cdunn/sequences/public/SRA/Haliotis_kamtschatkana/Haliotis_SRR536765.1.fastq.gz" --species "Haliotis kamtschatkana" --ncbi_id "6457" --itis_id "69494" --library_type "transcriptome" --note "Not trimmed, original reads are only 36bp" --sample_prep " | "
agalma catalog insert --id "454_ILYOBS" --paths "/gpfs/data/cdunn/sequences/public/SRA/Ilyanassa_Newbler/454Exemplar.fasta" --species "Ilyanassa obsoleta" --ncbi_id "34582" --itis_id "74169" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 data by Sonia from SRA archive" --sample_prep " | "
agalma catalog insert --id "454_LITLIT" --paths "/gpfs/data/cdunn/analyses/assembly/Littorina_littorea_newbler_ssmith_31Aug10/P_2010_06_14_14_49_46_runAssembly/assembly/454Exemplar.fna" --species "Littorina littorea" --ncbi_id "31216" --itis_id "70419" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 data by SSmith. Catalog exemplars used by SSmith in Mollusk paper" --sample_prep " | "
agalma catalog insert --id "JGI_LOTGI1" --paths "/gpfs/data/cdunn/sequences/public/JGI_August2012/Lottia/Lotgi1_GeneModels_FilteredModels1_nt.fasta" --species "Lottia gigantea" --ncbi_id "225164" --itis_id "69732" --library_type "genome"
agalma catalog insert --id "EAS1745-7-1-NEOMENIA" --paths "/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Neomenia_megatrapezata/s_7_1_sequence.q33.fastq.gz:/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Neomenia_megatrapezata/s_7_2_sequence.q33.fastq.gz" --species "Neomenia megatrapezata" --ncbi_id "1077245" --itis_id "205397" --extraction_id "NGW009" --library_id "NGW009" --library_type "transcriptome" --sequencer "Illumina Genome Analyzer IIx" --seq_center "Brown Genomics Core" --note "No trimming necessary" --sample_prep "Trizol | Invitrogen Dynabeads mRNA Purification Kit ; 1 round | NEBNext RNA Sample Prep. Reagent Set Set 1"
agalma catalog insert --id "ILLUMINA-FCBE01_0027_FC-3-OCTOPUS" --paths "/gpfs/data/cdunn/sequences/collaborators/mollusca_harvard_09Sep2011/Ovulgaris/Ovulgaris/s_3_1_sequence.q33.trim.fastq.gz:/gpfs/data/cdunn/sequences/collaborators/mollusca_harvard_09Sep2011/Ovulgaris/Ovulgaris/s_3_2_sequence.q33.trim.fastq.gz" --species "Octopus vulgaris" --ncbi_id "6645" --itis_id "82603" --library_type "transcriptome" --note "Trimmed to 120" --sample_prep "Trizol | Dynabeads mRNA Purification Kit | 2 rounds | Vollmer" --sequencer "Illumina Genome Analyzer II"
agalma catalog insert --id "454_PERLUC" --paths "/gpfs/data/cdunn/analyses/assembly/Perotrochus_lucaya_newbler_ssmith_31Aug10/P_2010_06_15_09_25_25_runAssembly/assembly/454Exemplar.fna" --species "Perotrochus lucaya" --ncbi_id "160000" --itis_id "69469" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 data by SSmith. Catalog exemplars used by SSmith in Mollusk paper" --sample_prep " | "
agalma catalog insert --id "OIST_PINFUC" --paths "/gpfs/data/cdunn/sequences/public/OIST/Pinctada_fucata/pfu_aug1.0_Nall.fasta" --species "Pinctada fucata" --ncbi_id "50426" --itis_id "79591" --library_type "genome" --note "ITIS corresponds to Pinctada" --sample_prep " | "
agalma catalog insert --id "454_SIPPEC" --paths "/gpfs/data/cdunn/analyses/assembly/Siphonaria_pectinata_newbler_ssmith_31Aug10/P_2010_06_16_09_32_27_runAssembly/assembly/454Exemplar.fna" --species "Siphonaria naufragum" --ncbi_id "57642" --itis_id "76445" --library_type "transcriptome" --sequencer "454" --note "Assembled 454 data by SSmith. Catalog exemplars used by SSmith in Mollusk paper" --sample_prep " | "
agalma catalog insert --id "EAS1745-1-SOLEMYA" --paths "/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Solemya_velum/s_1_1_sequence.q33.fastq.gz:/gpfs/data/cdunn/sequences/illumina/101105_EAS1745_00014_FC627UUAAXX_Freya_Goetz/reads_in_fastq_format/Solemya_velum/s_1_2_sequence.q33.fastq.gz" --species "Solemya velum" --ncbi_id "13268" --itis_id "79316" --extraction_id "FEG036" --library_id "FEG036" --library_type "transcriptome" --treatment "foot" --sequencer "Illumina Genome Analyzer IIx" --seq_center "Brown Genomics Core" --note "No trimming necessary" --sample_prep "Trizol | Invitrogen Dynabeads mRNA Purification Kit ; 2 rounds | NEBNext RNA Sample Prep. Master Mix Set 1"
