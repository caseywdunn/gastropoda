#!/bin/sh
#SBATCH -t 24:00:00
#SBATCH -c 16
#SBATCH --mem=60G
#SBATCH --exclusive

set -e

export BIOLITE_RESOURCES="database=/gpfs/data/cdunn/analyses/biolite-gastropoda.sqlite,agalma_database=/gpfs/data/cdunn/analyses/agalma-gastropoda.sqlite,outdir=/gpfs/data/cdunn/analyses/gastropoda,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

ID=ILLUMINA-FCBE01_0027_FC-5-HAMINOEA

echo $ID
agalma catalog search $ID

cd ~/scratch
mkdir -p $ID
cd $ID

agalma assemble --id $ID
