#!/usr/bin/env python

import os, sys
import random
import subprocess

basename="supermatrix.phy.BS"

for i in range(0,200):
        random_pars = random.randint(1, sys.maxint)
        cmd = 'raxmlHPC  -y -s {0}{1} -m PROTGAMMAWAG -n T{2} -p {3} -T 1'.format(
        basename, i, i, random_pars)
        print cmd
        subprocess.call(cmd, stdout=open(os.devnull, 'wb'), shell=True)

print "Done!"
