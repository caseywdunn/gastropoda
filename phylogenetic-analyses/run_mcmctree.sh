#!/bin/sh

#SBATCH -t 96:00:00
#SBATCH --mem=60G


set -e

#Refer to the MCMCTree Manual for running this analysis.
#Modify the control file accordingly and comment/uncomment the
#lines below as necessary. 
#This script assumes PAML is in your path

mcmctree mcmctree.ctl
codeml tmp1.ctl

echo "Done!"

