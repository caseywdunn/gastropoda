#Maximum likelihood bootstrap analysis
01-generate_bootstrap_reps.sh
02-generate_parsimony_trees.sh
03-run_examl.sh

#Bayesian phylogenetic analysis
run_phylobayes.sh

#Bayesian dating analysis
run_mcmctree.sh

#SOWH test
sowhat.sh
