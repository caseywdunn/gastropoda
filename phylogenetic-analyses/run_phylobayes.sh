#!/bin/bash

#SBATCH --nodes=8-8
#SBATCH --ntasks-per-node=4
#SBATCH -t 5-00:00:00
#SBATCH -n 32
#SBATCH --mem=60G

module load phylobayes/1.3b-mpi

srun pb_mpi -S -d supermatrix.phy -cat -poisson chain1_cat_pois
