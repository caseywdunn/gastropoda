          seed = -1
       seqfile = supermatrix.phy
      treefile = tree.tre
       outfile = out_run01

         ndata = 1
       seqtype = 2    * 0: nucleotides; 1:codons; 2:AAs
       usedata = 2    * 0: no data; 1:seq like; 2:normal approximation; 3:out.BV (in.BV)
         clock = 2    * 1: global clock; 2: independent rates; 3: correlated rates
       RootAge = '<5.5'  

         model = 0    * 0:JC69, 1:K80, 2:F81, 3:F84, 4:HKY85
         alpha = 0    * alpha for gamma rates at sites
         ncatG = 5    * No. categories in discrete gamma

     cleandata = 0    * remove sites with ambiguity data (1:yes, 0:no)?

       BDparas = 1 1 0.1   * birth, death, sampling
   kappa_gamma = 6 2      * gamma prior for kappa
   alpha_gamma = 1 1      * gamma prior for alpha

   rgene_gamma = 1 1     * gamma prior for rate for genes
  sigma2_gamma = 1 1    * gamma prior for sigma^2     (for clock=2 or 3)


      finetune = 1: .1 .1 .1 .1 .1 .1 * auto (0 or 1): times, rates, mixing, paras, RateParas, FossilErr

*      finetune = 1: 0.05  0.2  0.15  0.1 .5 * auto (0 or 1): times, rates, mixing, paras, RateParas, FossilErr

         print = 1
        burnin = 2000000
      sampfreq = 1000
       nsample = 10000


