#!/bin/bash

#SBATCH -t 10-00:00:00
#SBATCH -c 64

module load R/3.1.0;
module load raxml/8.0.22

perl sowhat --constraint=const.tre --aln=supermatrix.phy --model=PROTGAMMAWAG  --name=gastropods.orthogastropoda --rax="raxmlHPC-PTHREADS -T 64"  > gastropods.orthogastropoda --reps=900 --stop
