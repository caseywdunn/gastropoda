#!/bin/bash

#SBATCH -t 48:00:00
#SBATCH -c 16
#SBATCH -C e5-2600

module load raxml/8.0.14

raxmlHPC-HYBRID -m PROTGAMMAWAG -s supermatrix.phy -N 200 -n BS -f j -b $RANDOM -T 16

