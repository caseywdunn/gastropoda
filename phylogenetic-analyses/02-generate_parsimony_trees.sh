#!/bin/bash

#SBATCH -t 5:00:00
#SBATCH -c 1

module load raxml/8.0.14

python parsimony_trees.py
