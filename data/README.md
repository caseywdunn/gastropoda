## Phylogenetic data files for analyses presented in:

> Zapata F, Wilson NG, Howison M, Andrade SCS, Jörger KM, Schrödl M, Goetz FE, Giribet G, Dunn CW. Phylogenomics analyses of deepd gastropod relationships reject Orthogastropoda


There are 30 files corresponding to:

* Supermatrices 1, 2 and 3 for OMA and ABA strategies (6 files in total)
* Tree sets for maximum likelihood bootstrap replicates for each supermatrix (6 files in total)
* Maximum likelihood tree with bootstrap support values for each supermatrix (6 files in total)
* Tree sets for Bayesian inference (post-burn in) for each supermatrix (6 files in total)
* Majority rule consensus tree summarizing posterior probability for each supermatrix (6 files in total)

Files are named following this convention:

* Supermatrices: [OMA/ABA]_supermatrix[1/2/3].phy
* Tree sets for Maximum likelihood bootstrap replicates: [OMA/ABA]_supermatrix[1/2/3]_ML_BS_replicates.trees
* Maximum likelihood tree with bootstrap support values: [OMA/ABA]_supermatrix[1/2/3]_ML_Tree_BS_support.tre
* Tree sets for Bayesian inference: [OMA/ABA]_supermatrix[1/2/3]_BI_posterior_chains1_2.trees
* Majority rule consensus tree summarizing posterior probability: [OMA/ABA]_supermatrix[1/2/3]_BI_MRCT.tre

These files correspond to the figures presented in the paper:

* `OMA_supermatrix1_ML_Tree_BS_support.tre` is **Figure 3**
* `Chronogram.tre` is **Figure 4**
* `OMA_supermatrix2_ML_Tree_BS_support.tre` is **Supplementary Figure 1**
* `OMA_supermatrix3_ML_Tree_BS_support.tre` is **Supplementary Figure 2**
* `ABA_supermatrix1_BI_MRCT.tre` is **Supplementary Figure 3**

